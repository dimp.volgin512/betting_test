from datetime import datetime
from decimal import Decimal

from sqlalchemy import ForeignKey, TIMESTAMP
from sqlalchemy.orm import mapped_column, Mapped, declarative_base

Base = declarative_base()


class BetStatuses(Base):
    __tablename__ = "bet_statuses"

    PROCESSING = 1
    WIN = 2
    LOSE = 3

    id: Mapped[int] = mapped_column(primary_key=True)
    id_str: Mapped[str] = mapped_column(unique=True)


class Bets(Base):
    __tablename__ = "bets"

    id: Mapped[int] = mapped_column(primary_key=True)
    event_id: Mapped[int]
    bet_sum: Mapped[Decimal]
    created_at: Mapped[datetime] = mapped_column(type_=TIMESTAMP(timezone=True), default=datetime.now())
    status: Mapped[int] = mapped_column(ForeignKey("bet_statuses.id"))
