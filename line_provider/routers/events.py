from aio_pika import Channel
from fastapi import APIRouter, Depends, Path

from line_provider.controllers.events import EventsController
from line_provider.depends import get_controller
from line_provider.shemas.events import EventResponseSchema, EventCreateRequest, EventChangeRequest
from line_provider.utils import get_rabbit_channel

router = APIRouter(tags=["Events"])


@router.get("/")
async def get_all_events(
        controller: EventsController = Depends(get_controller(EventsController))
) -> list[EventResponseSchema]:
    return await controller.get_all(actual_events=True)


@router.get("/{event_id}/")
async def get_one_event(
        event_id: int = Path(...),
        controller: EventsController = Depends(get_controller(EventsController))
) -> EventResponseSchema:
    return await controller.get_by_id(id=event_id, actual_event=True)


@router.post("/")
async def create_event(
        body: EventCreateRequest,
        controller: EventsController = Depends(get_controller(EventsController))
) -> EventResponseSchema:
    return await controller.create(
        id=body.id,
        coefficient=body.coefficient,
        deadline_at=body.dead_line_at,
        status=body.status_id
    )


@router.put("/{event_id}/")
async def change_event(
        body: EventChangeRequest,
        event_id: int = Path(...),
        controller: EventsController = Depends(get_controller(EventsController)),
        rabbit_channel: Channel = Depends(get_rabbit_channel),
) -> EventResponseSchema:
    return await controller.change(
        id=event_id,
        coefficient=body.coefficient,
        deadline_at=body.dead_line_at,
        status=body.status_id,
        rabbit_channel=rabbit_channel
    )
