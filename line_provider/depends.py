from typing import Callable, TypeVar, Type, Awaitable, AsyncGenerator

from core.controllers import BaseController
from core.uow import SQLUnitOfWork
from fastapi import Depends
from line_provider.settings import get_settings, Settings
from sqlalchemy.ext.asyncio import AsyncSession
from starlette.requests import Request

Controller = TypeVar("Controller", bound=BaseController)


async def get_db_session(request: Request) -> AsyncGenerator[AsyncSession, None]:
    async with request.app.state.session_factory() as session:
        yield session


def get_controller(controller_class: Type[Controller]) -> Callable[[AsyncSession], Awaitable[Controller]]:
    async def _get_controller(
            session: AsyncSession = Depends(get_db_session),
            settings: Settings = Depends(get_settings)
    ) -> Callable[[AsyncSession], Awaitable[Controller]]:
        return controller_class(
            db_session=session,
            settings=settings,
            unit_of_work=SQLUnitOfWork(session),
        )
    return _get_controller
