from bet_maker.controllers.events import EventsController
from bet_maker.depends import get_controller
from bet_maker.shemas.events import EventResponseSchema
from fastapi import APIRouter, Depends

router = APIRouter(tags=["Events"])


@router.get("/")
async def get_all_events(
        controller: EventsController = Depends(get_controller(EventsController))
) -> list[EventResponseSchema]:
    return await controller.get_all()
