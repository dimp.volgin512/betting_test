from decimal import Decimal

from bet_maker.dao.bets import BetsDAO
from bet_maker.database_models import Bets
from bet_maker.services.events import EventsService
from bet_maker.shemas.bets import BetResponseSchema
from core.controllers import BaseController
from sqlalchemy.ext.asyncio import AsyncSession


class BetsController(BaseController):
    def __init__(self, db_session: AsyncSession, *args, **kwargs) -> None:
        super().__init__(db_session, *args, **kwargs)
        self._bets_dao = BetsDAO(db_session)
        self._events_service = EventsService(self._settings)

    async def get_all(self) -> list[BetResponseSchema]:
        return [BetResponseSchema.model_validate(event) for event in await self._bets_dao.get_all()]

    async def create(self, bet_sum: Decimal, event_id: int) -> Bets:
        await self._events_service.get_by_id(event_id=event_id)

        async with self._unit_of_work:
            return await self._bets_dao.create(bet_sum=bet_sum, event_id=event_id)
