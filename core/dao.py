from typing import Generic, TypeVar

from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import Query

_ModelT = TypeVar("_ModelT")


class BaseDAO(Generic[_ModelT]):
    def __init__(self, db_session: AsyncSession) -> None:
        self._db_session = db_session

    async def _get_one(self, query: Query) -> _ModelT:
        query_result = await self._db_session.execute(query)

        return query_result.scalar_one()

    async def _get_all(self, query: Query) -> list[_ModelT]:
        query_result = await self._db_session.execute(query)

        return query_result.scalars().fetchall()
