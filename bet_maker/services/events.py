from bet_maker.shemas.events import EventResponseSchema
from fastapi import HTTPException
from httpx import AsyncClient


class EventsService:

    def __init__(self, settings):
        self._settings = settings

    async def get_all(self) -> list[EventResponseSchema]:
        async with AsyncClient(timeout=1) as client:
            response = await client.get(
                url=f"http://{self._settings.LINE_PROVIDER_HOST}:{self._settings.LINE_PROVIDER_PORT}/events/"
            )
            if response.is_success:
                return [EventResponseSchema.model_validate(event) for event in response.json()]
            else:
                raise HTTPException(detail=response.json(), status_code=response.status_code)

    async def get_by_id(self, event_id: int) -> EventResponseSchema:
        async with AsyncClient(timeout=1) as client:
            response = await client.get(
                url=f"http://{self._settings.LINE_PROVIDER_HOST}:{self._settings.LINE_PROVIDER_PORT}/events/{event_id}/"
            )
            if response.is_success:
                return EventResponseSchema.construct(response.json())
            else:
                raise HTTPException(detail=response.json(), status_code=response.status_code)
