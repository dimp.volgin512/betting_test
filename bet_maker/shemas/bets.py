from pydantic import BaseModel, condecimal


class BetResponseSchema(BaseModel):
    id: int
    status: int
    event_id: int

    class Config:
        from_attributes = True


class BetCreateRequest(BaseModel):
    bet_sum: condecimal(gt=0, decimal_places=2)
    event_id: int
