from pathlib import Path

SETTINGS_PATH = Path(__file__).parent / ".env"
EVENT_QUEUE_NAME = "event"
