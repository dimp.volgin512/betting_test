from bet_maker.controllers.bets import BetsController
from bet_maker.depends import get_controller
from bet_maker.shemas.bets import BetResponseSchema, BetCreateRequest
from fastapi import APIRouter, Depends

router = APIRouter(tags=["Bets"])


@router.get("/")
async def get_all_bets(
    controller: BetsController = Depends(get_controller(BetsController))
) -> list[BetResponseSchema]:
    return await controller.get_all()


@router.post("/")
async def create_bet(
    body: BetCreateRequest,
    controller: BetsController = Depends(get_controller(BetsController))
) -> BetResponseSchema:
    return await controller.create(bet_sum=body.bet_sum, event_id=body.event_id)
