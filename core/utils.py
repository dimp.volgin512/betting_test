import logging

from pydantic_settings import BaseSettings
from sqlalchemy.ext.asyncio import AsyncEngine, create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker


def create_engine(settings: BaseSettings) -> tuple[AsyncEngine, sessionmaker]:
    engine = create_async_engine(
        settings.DATABASE_URL,
        pool_size=settings.POOL_SIZE,
        max_overflow=settings.MAX_OVERFLOW,
    )
    logging.debug(f"Установлено подключение к бд: {settings.PG_NAME}")

    session_maker = sessionmaker(engine, expire_on_commit=False, class_=AsyncSession)
    return engine, session_maker
