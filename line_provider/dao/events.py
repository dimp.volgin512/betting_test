from datetime import datetime
from decimal import Decimal

from core.dao import BaseDAO
from fastapi import HTTPException
from line_provider.database_models import Events
from sqlalchemy import select
from sqlalchemy.exc import IntegrityError, NoResultFound
from starlette import status


class EventDAO(BaseDAO):
    async def get_all(self, event_deadline: datetime | None) -> list[Events]:
        query = select(Events)
        if event_deadline:
            query = query.where(Events.dead_line_at > event_deadline)

        return await self._get_all(query)

    async def get_one(self, id: int, event_deadline: datetime | None = None) -> Events:
        query = select(Events).where(Events.id == id)
        if event_deadline:
            query = query.where(Events.dead_line_at > event_deadline)

        try:
            return await self._get_one(query)
        except NoResultFound:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"event {id} not found or ended")

    async def create(
        self,
        id: int,
        coefficient: Decimal | None,
        deadline_at: datetime | None,
        event_status: int | None
    ) -> Events:
        event = Events(id=id, coefficient=coefficient, dead_line_at=deadline_at, status=event_status)
        self._db_session.add(event)

        try:
            await self._db_session.flush()
        except IntegrityError as e:
            if e.orig.sqlstate == "23505":
                raise HTTPException(
                    status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                    detail="Event with received id is already exists"
                )
            else:
                raise e

        return event
