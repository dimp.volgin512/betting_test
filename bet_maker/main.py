from bet_maker.database_models import Base
from bet_maker.routers.bets import router as bet_router
from bet_maker.routers.events import router as event_router
from bet_maker.settings import get_settings
from bet_maker.utils import rabbit_run, get_rabbit_close_event, init_tables
from core.utils import create_engine
from fastapi import FastAPI
from uvicorn import run


def main() -> None:
    settings = get_settings()
    run(
        create_app(),
        host=settings.APP_HOST,
        port=settings.APP_PORT,
    )


def create_app() -> FastAPI:
    settings = get_settings()

    app = FastAPI(title=settings.TITLE)

    engine, session_factory = create_engine(settings=settings)

    app.state.settings = settings
    app.state.session_factory = session_factory

    app.add_event_handler("startup", init_tables(engine, Base))
    app.add_event_handler("startup", rabbit_run(app))

    app.add_event_handler("shutdown", get_rabbit_close_event(app))
    app.add_event_handler("shutdown", engine.dispose)

    app.include_router(event_router, prefix="/events")
    app.include_router(bet_router, prefix="/bets")
    return app
