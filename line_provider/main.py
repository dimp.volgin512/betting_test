from core.utils import create_engine
from fastapi import FastAPI
from line_provider.database_models import Base
from line_provider.routers.events import router as event_router
from line_provider.settings import get_settings
from line_provider.utils import get_rabbit_close_event, get_rabbit_setup_event, init_tables
from uvicorn import run


def main() -> None:
    settings = get_settings()
    run(
        create_app(),
        host=settings.APP_HOST,
        port=settings.APP_PORT,
    )


def create_app() -> FastAPI:
    settings = get_settings()

    app = FastAPI(title=settings.TITLE)

    engine, session_factory = create_engine(settings=settings)

    app.state.settings = settings
    app.state.session_factory = session_factory

    app.add_event_handler("startup", init_tables(engine, Base))
    app.add_event_handler("startup", get_rabbit_setup_event(app))

    app.add_event_handler("shutdown", get_rabbit_close_event(app))
    app.add_event_handler("shutdown", engine.dispose)

    app.include_router(event_router, prefix="/events")
    return app
