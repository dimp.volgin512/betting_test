from datetime import datetime
from decimal import Decimal

from pydantic import BaseModel, condecimal


class EventResponseSchema(BaseModel):
    id: int
    coefficient: Decimal | None
    dead_line_at: datetime | None
    status: int | None

    class Config:
        from_attributes = True


class EventCreateRequest(BaseModel):
    id: int
    coefficient: condecimal(gt=0, decimal_places=2) | None
    dead_line_at: datetime | None
    status_id: int | None


class EventChangeRequest(BaseModel):
    coefficient: condecimal(gt=0, decimal_places=2) | None = None
    dead_line_at: datetime | None = None
    status_id: int | None = None


class EventChangeStatusMessage(BaseModel):
    event_id: int
    status_id: int
