from decimal import Decimal

from bet_maker.database_models import Bets, BetStatuses
from core.dao import BaseDAO
from sqlalchemy import select


class BetsDAO(BaseDAO):
    async def get_all(self, event_id: int | None = None) -> list[Bets]:
        query = select(Bets)
        if event_id:
            query = query.where(Bets.event_id == event_id)

        return await self._get_all(query)

    async def create(self, bet_sum: Decimal, event_id: int) -> Bets:
        bet = Bets(bet_sum=bet_sum, status=BetStatuses.PROCESSING, event_id=event_id)
        self._db_session.add(bet)
        return bet
