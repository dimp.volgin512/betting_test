from bet_maker.services.events import EventsService
from bet_maker.shemas.events import EventResponseSchema
from core.controllers import BaseController
from sqlalchemy.ext.asyncio import AsyncSession


class EventsController(BaseController):

    def __init__(self, db_session: AsyncSession, *args, **kwargs) -> None:
        super().__init__(db_session, *args, **kwargs)
        self._events_service = EventsService(self._settings)

    async def get_all(self) -> list[EventResponseSchema]:
        return await self._events_service.get_all()
