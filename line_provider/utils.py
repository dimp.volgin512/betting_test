import logging
from typing import Callable, Awaitable

import aio_pika
from aio_pika.abc import AbstractChannel
from fastapi import FastAPI
from line_provider.database_models import EventStatuses
from sqlalchemy import insert
from sqlalchemy.orm import DeclarativeMeta
from starlette.requests import Request


def get_rabbit_setup_event(app: FastAPI) -> Callable[[], Awaitable[None]]:
    settings = app.state.settings

    async def event() -> None:
        connection = await aio_pika.connect_robust(
            host=settings.RABBIT_HOST,
            virtualhost=settings.RABBIT_VHOST,
            login=settings.RABBIT_USER,
            password=settings.RABBIT_PASS
        )
        channel = await connection.channel()

        app.state.rabbit_channel = channel
        app.state.rabbit_connection = connection
        logging.info(f"Установленно соединение с RabbitMQ")

    return event


def get_rabbit_close_event(app: FastAPI) -> Callable[[], Awaitable[None]]:
    async def event() -> None:
        await app.state.rabbit_connection.close()
        logging.info("Закрыто соединение с RabbitMQ")
    return event


def get_rabbit_channel(request: Request) -> AbstractChannel:
    return request.app.state.rabbit_channel


def init_tables(engine, base: DeclarativeMeta) -> Callable[[None], Awaitable[None]]:
    async def init():
        async with engine.begin() as conn:
            await conn.run_sync(base.metadata.drop_all)      # TODO
            await conn.run_sync(base.metadata.create_all)
            stmt = insert(EventStatuses).values(
                [
                    {"id_str": "PROCESSING"},
                    {"id_str": "WIN_FIRST"},
                    {"id_str": "WIN_SECOND"}
                ]
            )
            await conn.execute(stmt)
            await conn.commit()
    return init
