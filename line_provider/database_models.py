from datetime import datetime
from decimal import Decimal

from sqlalchemy import ForeignKey, TIMESTAMP
from sqlalchemy.orm import mapped_column, Mapped, declarative_base

Base = declarative_base()


class EventStatuses(Base):
    __tablename__ = "line_statuses"

    PROCESSING = 1
    WIN_FIRST = 2
    WIN_SECOND = 3

    id: Mapped[int] = mapped_column(primary_key=True)
    id_str: Mapped[str] = mapped_column(unique=True)


class Events(Base):
    __tablename__ = "events"

    id: Mapped[int] = mapped_column(primary_key=True)
    coefficient: Mapped[Decimal | None]
    dead_line_at: Mapped[datetime | None] = mapped_column(type_=TIMESTAMP(timezone=True))
    status: Mapped[int | None] = mapped_column(ForeignKey("line_statuses.id"))
