from datetime import datetime
from decimal import Decimal

from pydantic import BaseModel


class EventResponseSchema(BaseModel):
    id: int
    coefficient: Decimal | None
    dead_line_at: datetime | None
    status: int | None
