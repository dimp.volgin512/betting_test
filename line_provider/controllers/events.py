from datetime import datetime
from decimal import Decimal

import aio_pika
from aio_pika import Channel
from core.controllers import BaseController
from line_provider.constants import EVENT_QUEUE_NAME
from line_provider.dao.events import EventDAO
from line_provider.database_models import Events
from line_provider.shemas.events import EventResponseSchema, EventChangeStatusMessage
from sqlalchemy.ext.asyncio import AsyncSession


class EventsController(BaseController):

    @staticmethod
    async def _schedule_message(rabbit_channel: Channel, message: EventChangeStatusMessage | None) -> None:
        await rabbit_channel.default_exchange.publish(
            aio_pika.Message(
                body=message.json().encode(),
                delivery_mode=aio_pika.DeliveryMode.PERSISTENT,
            ),
            routing_key=EVENT_QUEUE_NAME,
        )

    def __init__(self, db_session: AsyncSession, *args, **kwargs) -> None:
        super().__init__(db_session, *args, **kwargs)
        self._event_dao = EventDAO(db_session)

    async def get_all(self, actual_events: bool | None = False) -> list[EventResponseSchema]:
        events = await self._event_dao.get_all(event_deadline=datetime.now() if actual_events else None)
        return [EventResponseSchema.model_validate(event) for event in events]

    async def get_by_id(self, id: int, actual_event: bool | None = False):
        return await self._event_dao.get_one(id, event_deadline=datetime.now() if actual_event else None)

    async def create(
        self,
        id: int,
        coefficient: Decimal | None,
        deadline_at: datetime | None,
        status: int | None
    ) -> Events:
        async with self._unit_of_work:
            return await self._event_dao.create(
                id=id,
                coefficient=coefficient,
                deadline_at=deadline_at,
                event_status=status
            )

    async def change(
        self,
        id: int,
        coefficient: Decimal | None,
        deadline_at: datetime | None,
        status: int | None,
        rabbit_channel: Channel
    ) -> Events:
        message = None

        async with self._unit_of_work:
            event = await self._event_dao.get_one(id)
            if coefficient:
                event.coefficient = coefficient
            if deadline_at:
                event.dead_line_at = deadline_at
            if status:
                if event.status != status:
                    message = EventChangeStatusMessage(event_id=id, status_id=status)
                event.status = status

        if message:
            await self._schedule_message(rabbit_channel=rabbit_channel, message=message)
        return event
