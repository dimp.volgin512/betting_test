import json
import logging
from enum import auto, IntEnum
from typing import Callable, Awaitable

import aio_pika
from aio_pika.abc import AbstractIncomingMessage
from bet_maker.constants import EVENT_QUEUE_NAME
from bet_maker.dao.bets import BetsDAO
from bet_maker.database_models import BetStatuses
from core.uow import SQLUnitOfWork
from fastapi import FastAPI
from pydantic import BaseModel, ValidationError
from sqlalchemy import insert
from sqlalchemy.orm import DeclarativeMeta


class EventStatusesEnum(IntEnum):
    PROCESSING = auto()
    WIN_FIRST = auto()
    WIN_SECOND = auto()


class EventMessage(BaseModel):
    event_id: int
    status_id: int


def _get_callback(app: FastAPI) -> Callable[[AbstractIncomingMessage], Awaitable[None]]:
    async def callback(message: AbstractIncomingMessage) -> None:
        async with message.process(), app.state.session_factory() as session:
            async with SQLUnitOfWork(session):

                message = EventMessage.model_validate(json.loads(message.body))
                bets = await BetsDAO(session).get_all(message.event_id)

                for bet in bets:
                    match message.status_id:
                        case EventStatusesEnum.WIN_FIRST:
                            bet.status = BetStatuses.WIN
                        case EventStatusesEnum.WIN_SECOND:
                            bet.status = BetStatuses.LOSE
                        case EventStatusesEnum.PROCESSING:
                            bet.status = BetStatuses.PROCESSING
                        case _:
                            raise ValidationError
    return callback


def rabbit_run(app: FastAPI) -> Callable[[None], Awaitable[None]]:
    settings = app.state.settings

    async def main() -> None:
        connection = await aio_pika.connect_robust(
            host=settings.RABBIT_HOST,
            virtualhost=settings.RABBIT_VHOST,
            login=settings.RABBIT_USER,
            password=settings.RABBIT_PASS
        )
        queue_name = EVENT_QUEUE_NAME
        channel = await connection.channel()
        queue = await channel.declare_queue(queue_name, auto_delete=True)

        app.state.rabbit_channel = channel
        app.state.rabbit_connection = connection

        await queue.consume(_get_callback(app))
        logging.info(f"Установленно соединение с RabbitMQ")

    return main


def get_rabbit_close_event(app: FastAPI) -> Callable[[], Awaitable[None]]:
    async def event() -> None:
        await app.state.rabbit_connection.close()
        logging.info("Закрыто соединение с RabbitMQ")
    return event


def init_tables(engine, base: DeclarativeMeta) -> Callable[[None], Awaitable[None]]:
    async def init():
        async with engine.begin() as conn:
            await conn.run_sync(base.metadata.drop_all)
            await conn.run_sync(base.metadata.create_all)
            stmt = insert(BetStatuses).values(
                [
                    {"id_str": "PROCESSING"},
                    {"id_str": "WIN"},
                    {"id_str": "LOSE"}
                ]
            )
            await conn.execute(stmt)
            await conn.commit()
    return init

