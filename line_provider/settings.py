from functools import lru_cache

from line_provider.constants import SETTINGS_PATH
from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    PG_HOST: str
    PG_PORT: int
    PG_USER: str
    PG_PASS: str
    PG_NAME: str
    POOL_SIZE: int = 10
    MAX_OVERFLOW: int = 10

    RABBIT_HOST: str
    RABBIT_VHOST: str
    RABBIT_USER: str
    RABBIT_PASS: str

    APP_HOST: str = "0.0.0.0"
    APP_PORT: int = 8000

    TITLE: str = 'LINE PROVIDER'

    @property
    def DATABASE_URL(self):
        return f'postgresql+asyncpg://{self.PG_USER}:{self.PG_PASS}@{self.PG_HOST}:{self.PG_PORT}/{self.PG_NAME}'

    class Config:
        env_file = SETTINGS_PATH


@lru_cache
def get_settings() -> Settings:
    return Settings()
